<?php

declare(strict_types=1);

namespace JaAdmin\TextBlockModule\Models;

use JaAdmin\CoreModule\Models\BaseEntity;
use JsemAjtacka\TextBlock\TextBlockTranslation as TextBlockTranslationInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslationInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslationTrait;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[ORM\Table(name: "text_block_translation")]
class TextBlockTranslation extends BaseEntity implements TextBlockTranslationInterface
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private int $id;

    #[ORM\Column(type: "string")]
    protected string $content;

    public function getId(): int
    {
        return $this->id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }
}
