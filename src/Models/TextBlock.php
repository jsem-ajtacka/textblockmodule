<?php

declare(strict_types=1);

namespace JaAdmin\TextBlockModule\Models;

use JaAdmin\CoreModule\Models\BaseEntity;
use JsemAjtacka\TextBlock\TextBlock as TextBlockInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[ORM\Table(name: "text_block")]
class TextBlock extends BaseEntity implements TextBlockInterface
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private int $id;

    #[ORM\Column(type: "string", unique: true)]
    protected string $slug;

    public function getId(): int
    {
        return $this->id;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }
}
