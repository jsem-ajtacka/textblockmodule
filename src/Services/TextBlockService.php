<?php

declare(strict_types=1);

namespace JaAdmin\TextBlockModule\Services;

use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use JaAdmin\CoreModule\Models\EntityManagerDecorator;
use JaAdmin\CoreModule\Services\SettingsService;
use JaAdmin\CoreModule\Utils\KW;
use JaAdmin\TextBlockModule\Models\TextBlock;
use JsemAjtacka\TextBlock\TextBlockService as TextBlockServiceInterface;
use Nette\Utils\ArrayHash;

class TextBlockService implements TextBlockServiceInterface
{
    private ObjectRepository $repository;
    private array $locales;

    public function __construct(private EntityManagerDecorator $em, private SettingsService $settingsService)
    {
        $this->repository = $this->em->getRepository(TextBlock::class);
        $this->locales = $this->settingsService->settings["website"]["locales"];
    }

    public function getItem(int $id) : TextBlock|null
    {
        return $this->repository->findOneBy([KW::Id => $id]);
    }

    public function getItemBySlug(string $slug) : TextBlock|null
    {
        return $this->repository->findOneBy([KW::Slug => $slug]);
    }

    public function getItems()
    {
        return $this->repository->createQueryBuilder(KW::Entity, KW::Entity . "." . KW::Id)->getQuery()->getResult();
    }

    /**
     * @throws Exception
     */
    public function addItem(ArrayHash $values) : TextBlock
    {
        $textBlock = new TextBlock();
        return $this->saveItem($textBlock, $values);
    }

    /**
     * @throws Exception
     */
    public function editItem(ArrayHash $values) : TextBlock
    {
        $textBlock = $this->getItem($values->id);
        return $this->saveItem($textBlock, $values);
    }

    public function delete(string $id): void
    {
        $item = $this->getItem($id);
        $this->em->remove($item);
        $this->em->flush();
    }

    private function saveItem(TextBlock $textBlock, ArrayHash $values): TextBlock
    {
        $textBlock->setSlug($values->slug);

        foreach($this->locales as $locale) {
            $textBlock->translate($locale)->setContent($values->{"content_" . $locale});
        }

        $this->em->persist($textBlock);
        $textBlock->mergeNewTranslations();
        $this->em->flush();

        return $textBlock;
    }

    public function getQueryBuilder(): QueryBuilder
    {
        return $this->repository->createQueryBuilder(KW::Entity, KW::Entity . KW::Dot . KW::Id);
    }

    public function getTotalItemsCount(): int
    {
        return $this->getQueryBuilder()->select('COUNT(' . KW::Entity . ')')->getQuery()->getSingleScalarResult();
    }
}
