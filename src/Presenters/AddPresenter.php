<?php

declare(strict_types=1);

namespace JaAdmin\TextBlockModule\Presenters;

use Exception;
use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use JaAdmin\CoreModule\Utils\Privilege;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;
use Tracy\Debugger;
use Tracy\ILogger;

final class AddPresenter extends BasePresenter
{
    private const RedirectLink = ":TextBlock:Overview:default";
    private const PermissionAddFail = "textBlockModule.add.flashMessage.permissionFail";
    private const AddSuccess = "textBlockModule.add.flashMessage.addSuccess";
    private const AddFail = "textBlockModule.add.flashMessage.addFail";

    public function actionDefault()
    {
        $isUserAllowed = $this->getUser()->isAllowed(self::ExtensionName, Privilege::Add);

        if (!$isUserAllowed) {
            $this->flashMessage(self::PermissionAddFail);
            $this->redirect(self::RedirectLink);
        }
    }

    public function createComponentAddForm(): Form
    {
        $form = new Form();

        $form->setTranslator($this->translator);

        $form->addText("slug", "textBlockModule.add.form.slug.label")
            ->setRequired();

        foreach($this->locales as $locale) {
            $form->addTextarea("content_" . $locale, $this->translator->translate("textBlockModule.add.form.content.label", ["locale" => Strings::upper($locale)]));
        }

        $form->addSubmit("submit", "textBlockModule.add.form.submit.label");

        $form->onSuccess[] = [$this, "addFormSubmitSuccess"];

        return $form;
    }

    public function addFormSubmitSuccess(Form $form, ArrayHash $values)
    {
        try {
            $this->textBlockService->addItem($values);
            $this->flashMessage(new FlashMessage(self::AddSuccess, FlashMessageType::Success));
        } catch (Exception $e) {
            Debugger::log($e->getMessage(), ILogger::EXCEPTION);
            $this->flashMessage(new FlashMessage(self::AddFail, FlashMessageType::Danger));
        }

        $this->redirect(self::RedirectLink);
    }
}
