<?php

declare(strict_types=1);

namespace JaAdmin\TextBlockModule\Presenters;

use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use JaAdmin\CoreModule\Utils\Privilege;

final class OverviewPresenter extends BasePresenter
{
    private const RedirectLink = ":TextBlock:Overview:default";
    private const DeleteSuccess = "textBlockModule.overview.flashMessage.deleteSuccess";

    public function renderDefault()
    {
        $isUserAllowedToAdd = $this->getUser()->isAllowed(self::ExtensionName, Privilege::Add);
        $this->template->isUserAllowedToAdd = $isUserAllowedToAdd;

        $isUserAllowedToEdit = $this->getUser()->isAllowed(self::ExtensionName, Privilege::Edit);
        $this->template->isUserAllowedToEdit = $isUserAllowedToEdit;

        $isUserAllowedToDelete = $this->getUser()->isAllowed(self::ExtensionName, Privilege::Delete);
        $this->template->isUserAllowedToDelete = $isUserAllowedToDelete;

        if (!isset($this->template->items)) {
            $this->template->items = $this->textBlockService->getItems();
        }
    }

    public function handleDelete(string $id)
    {
        $this->textBlockService->delete($id);
        $this->flashMessage(new FlashMessage(self::DeleteSuccess, FlashMessageType::Success));
        $this->redirect(self::RedirectLink);
    }
}
