<?php

declare(strict_types=1);

namespace JaAdmin\TextBlockModule\Presenters;

use JaAdmin\CoreModule\Presenters\SecurePresenter;
use JaAdmin\CoreModule\Services\SettingsService;
use JaAdmin\TextBlockModule\Services\TextBlockService;
use Nette\DI\Attributes\Inject;

class BasePresenter extends SecurePresenter
{
    protected const ExtensionName = "textBlock";
    private const RedirectLink = ":Core:Overview:default";

    #[Inject]
    public TextBlockService $textBlockService;

    #[Inject]
    public SettingsService $settingsService;

    protected $locales;

    public function startup()
    {
        parent::startup();

        $this->locales = $this->settingsService->settings["website"]["locales"];
        $this->template->locales = $this->locales;

        if (!$this->extensionService->getItem(self::ExtensionName)->isActive()) {
            $this->redirect(self::RedirectLink);
        }
    }
}
