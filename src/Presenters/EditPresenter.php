<?php

declare(strict_types=1);

namespace JaAdmin\TextBlockModule\Presenters;

use Exception;
use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use JaAdmin\CoreModule\Utils\Privilege;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;
use Tracy\Debugger;
use Tracy\ILogger;

final class EditPresenter extends BasePresenter
{
    private const RedirectLink = ":TextBlock:Overview:default";
    private const PermissionFail = "textBlockModule.edit.flashMessage.permissionFail";
    private const EditSuccess = "textBlockModule.edit.flashMessage.editSuccess";
    private const EditFail = "textBlockModule.edit.flashMessage.editFail";

    public string $id;

    public function actionDefault(string $id)
    {
        $isUserAllowed = $this->getUser()->isAllowed(self::ExtensionName, Privilege::Edit);

        if (!$isUserAllowed) {
            $this->flashMessage(self::ErrorPermissionFail);
            $this->redirect(self::RedirectLink);
        }

        $this->id = $id;
    }

    public function createComponentEditForm(): Form
    {
        $form = new Form();

        $form->setTranslator($this->translator);

        $form->addText("slug", "textBlockModule.edit.form.slug.label")
            ->setDisabled()
            ->setOmitted(false);

        foreach($this->locales as $locale) {
            $form->addTextarea("content_" . $locale, $this->translator->translate("textBlockModule.edit.form.content.label", ["locale" => Strings::upper($locale)]));
        }

        $form->addSubmit("submit", "textBlockModule.edit.form.submit.label");

        if (!empty($this->id)) {
            $item = $this->textBlockService->getItem($this->id);

            $defaults = [
                "slug" => $item->getSlug()
            ];

            foreach ($this->locales as $locale) {
                $defaults["content_" . $locale] = $item->translate($locale)->getContent();
            }

            $form->setDefaults($defaults);
        }

        $form->onSuccess[] = [$this, "editFormSubmitSuccess"];

        return $form;
    }

    public function editFormSubmitSuccess(Form $form, ArrayHash $values)
    {
        try {
            $values->id = $this->id;
            $this->textBlockService->editItem($values);
            $this->flashMessage(new FlashMessage(self::EditSuccess, FlashMessageType::Success));
        } catch (Exception $e) {
            Debugger::log($e->getMessage(), ILogger::EXCEPTION);
            $this->flashMessage(new FlashMessage(self::EditFail, FlashMessageType::Danger));
        }

        $this->redirect(self::RedirectLink);
    }
}
