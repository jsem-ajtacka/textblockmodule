<?php

declare(strict_types=1);

namespace JaAdmin\TextBlockModule\DI;

use Nette\DI\CompilerExtension;

final class TextBlockModuleExtension extends CompilerExtension {}